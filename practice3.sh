#!/bin/bash
i=1
x=10
while [ $i -le $x ]
do
	t=1
	str=""
	while [ $t -le 10 ]
	do
		str=$str" "$(( t*i ))
		t=$(($t+1))
	done
	echo $str
	i=$(($i+1))
done
