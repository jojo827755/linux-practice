read -p "Enter a : " a 
#enter b 
read -p "Enter b : " b 
#enter c
read -p "Enter c : " c
if [ $a -eq 0 ]
then
	if [ $b -eq 0 ]
	then
		if [ $c -eq 0 ]
		then
			echo " infinite "
		else
			echo " none solutions "
		fi
	
	else 
		x=`echo " scale=2; -$c/$b " | bc`
		echo "one real solution x= $x"
		
	fi
else	
	#calculate delta
	delta=$(($b*$b-4*$a*$c))

	if [ $delta -gt 0 ]
	then
		#delta > 0 then 
		echo -e delta = $delta positive, there are 2 real solutions
		x1=`echo "scale=4; (-$b+sqrt($delta))/(2*$a)" | bc`	
		x2=`echo "scale=4; (-$b-sqrt($delta))/(2*$a)" | bc`
		echo "x1 = $x1"
		echo "x2 = $x2"
	elif [ $delta -eq 0 ] 
	then
		#delta = 0 
		echo -e delta = $delta zero, there is one real solution
		x=`echo "-$b/(2*$a)" | bc`
		echo "x = $x"
	elif [ $delta -lt 0 ]
	then
		#delta # 0
		echo -e delta = $delta negative, there are 2 complex solutions
	fi
fi

